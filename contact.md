---
title: Contact and Helpdesk
title_image: default
layout: default
excerpt:
    Contact to HIFIS, Helpdesk.
lang: en
lang_ref: contact
additional_css:
  - contact.css
---

## Contact us!
#### Contact us for queries on HIFIS and Helmholtz Cloud, general questions and also feedback to the website:
{% include contact_form.html %}

## You can also reach us via mail:
[support@hifis.net](mailto:{{ site.contact_mail }})

{:.text-success}
## Core Contact Team
Please refer the our [Team page]({% link team/index.md %}) for all corresponding partners for HIFIS.
The core team for contact consists of:

#### Uwe Jandt

HIFIS Office Management

[Deutsches Elektronen-Synchrotron (DESY)](https://www.desy.de/)


#### Annette Spicker

Cloud Cluster Management

[Helmholtz-Zentrum Berlin für Materialien und Energie (HZB)](https://www.helmholtz-berlin.de/)


#### Tobias Huste

Software Cluster Management

[Helmholtz-Zentrum Dresden-Rossendorf (HZDR)](https://www.hzdr.de/)


