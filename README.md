# Helmholtz AAI web page

The Helmholtz AAI web page will be the home for gathering information about
the activities and services offered by the Helmholtz AAI team. This website
is built using Jekyll.

It will be deployed at <https://aai.helmholtz.de>.

## Build the project locally

1. Clone the project locally
   ```bash
   git clone --recursive https://gitlab.hzdr.de/helmholtz-aai/aai.helmholtz.de.git
   cd aai.helmholtz.de
   ```
2. [Install](https://jekyllrb.com/docs/installation/) Jekyll.
3. Install dependencies.
   ```bash
   bundle install
   ```
4. Install submodules (currently `bootstrap`, `Font-Awesome`, `jquery`, `MathJax`).
   ``` bash
   git submodule update --init --recursive
   ```
5. Build and preview the web page.
   ```bash
   bundle exec jekyll serve --future
   ```
   Alternatively, you may build with a given base folder and serve where you want:
   ```bash
   bundle exec jekyll build --future --baseurl "/subfolderofyourchoice"
   ```
6. Add content.

Execute the commands above from the root of your repository.

More information is available in Jekyll's
[documentation](https://jekyllrb.com/docs/)

## Fetch the latest builds from gitlab

The latest master branch (production) build artifacts can be downloaded using
* https://gitlab.hzdr.de/helmholtz-aai/aai.helmholtz.de/-/jobs/artifacts/master/download?job=build:production
