---
title: Helmholtz AAI
title_image: default
layout: frontpage
additional_js: frontpage.js
redirect_to: https://hifis.net/aai
excerpt:
  "Helmholtz AAI provides seamless access to cloud services from and for Helmholtz and beyond.
  It is built and maintained by HIFIS.
  This page links to HIFIS.net and the corresponding AAI service sites."
---
{% comment %}
  This markdown file triggers the generation of the frontpage.
  Only the frontmatter is required by Jekyll.
  The contents section does not get rendered into HTML on purpose.
{% endcomment %}
