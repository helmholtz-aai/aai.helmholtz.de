---
title: "Helmholtz AAI full presentation in 75. DFN Betriebstagung"
title_image: default
data: 2021-10-22
layout: blogpost
categories:
  - news
redirect_to:
  - https://hifis.net/news/2021/10/22/helmholtz-aai-full-presentation-in-dfn-meeting
excerpt: >
    After a brief pitch in the previous meeting, Helmholtz AAI is being presented in full breadth in the 75. DFN Betriebstagung.
---
