---
title: "Helmholtz AAI Update in the context of AARC, EOSC, NFDI"
title_image: default
data: 2021-04-14
layout: blogpost
categories:
  - news
redirect_to:
  - https://hifis.net/news/2021/04/14/helmholtz-aai-in-nfdi-meeting
excerpt: >
    Helmholtz AAI is briefly pitched in the 74. DFN Betriebstagung.
---
