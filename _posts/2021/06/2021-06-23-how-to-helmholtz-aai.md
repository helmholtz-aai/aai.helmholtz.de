---
title: "AAI login to Helmholtz Cloud and Associated Services"
title_image: default
data: 2021-06-23
layout: blogpost
categories:
  - tutorial
redirect_to:
  - https://hifis.net/tutorial/2021/06/23/how-to-helmholtz-aai
excerpt: >
    Pictured tutorial on usage of Helmholtz AAI login and registration procedures for Helmholtz Cloud and Associated services, using the HIFIS Events Management Platform at <a href="https://events.hifis.net">events.hifis.net</a> as an example.
---
