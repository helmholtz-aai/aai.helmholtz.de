---
title: "16 of 18 Helmholtz centres plus Head Office connected"
title_image: default
data: 2021-02-09
layout: blogpost
categories:
  - news
redirect_to:
  - https://hifis.net/news/2021/02/09/connected-centres-update-including-hgs
excerpt: >
    16 of 18 Helmholtz centres plus Head Office meanwhile have an IdP in Helmholtz AAI and thus joined the big EduGAIN community.
---
