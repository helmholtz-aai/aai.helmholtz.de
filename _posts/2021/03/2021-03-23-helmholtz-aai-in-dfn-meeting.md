---
title: "Helmholtz AAI in 74. DFN Betriebstagung"
title_image: default
data: 2021-03-23
layout: blogpost
categories:
  - news
redirect_to:
  - https://hifis.net/news/2021/03/23/helmholtz-aai-in-dfn-meeting
excerpt: >
    Helmholtz AAI is briefly pitched in the 74. DFN Betriebstagung.
---
