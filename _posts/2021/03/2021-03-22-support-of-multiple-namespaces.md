---
title: "Support of multiple namespaces in entitlement"
title_image: default
data: 2021-03-22
layout: blogpost
categories:
  - news
redirect_to:
  - https://hifis.net/news/2021/03/22/support-of-multiple-namespaces
excerpt: >
    The Helmholtz AAI supports the usage of multiple namespaces in entitlements. For now, VO membership information can be created using different namespaces.

---
