---
title: "Helmholtz AAI supports PUNCH4NFDI"
title_image: default
data: 2021-08-24
authors:
  - "Sander Apweiler"
  - "Marcus Hardt"
  - "Uwe Jandt"
layout: blogpost
categories:
  - news
redirect_to:
  - https://hifis.net/use-case/2022/06/28/use-case-PUNCH
excerpt: >
    The Helmholtz AAI supports PUNCH4NFDI by providing AAI components. This offers a working solution on a strongly connected AAI, which follows already the recommendations and guidelines for participating in the EOSC.
---
