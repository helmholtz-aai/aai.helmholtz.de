---
title: "New Guidelines for Helmholtz AAI Participants"
title_image: default
data: 2021-08-19
layout: blogpost
categories:
  - news
redirect_to:
  - https://hifis.net/news/2021/08/19/new-guidelines
excerpt: >
    We renewed the <a href="https://hifis.net/doc/helmholtz-aai/">Helmholtz AAI documentation</a> and provide guidelines and how-to pages for different participants of Helmholtz AAI:  Users, Service Providers, Group Managers, etc.
---
