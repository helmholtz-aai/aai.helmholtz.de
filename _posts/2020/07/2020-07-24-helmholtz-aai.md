---
title: "Helmholtz AAI online"
title_image: default
data: 2020-07-24
authors:
  - jandt
layout: blogpost
categories:
  - news
excerpt_separator: <!--more-->
---

The blueprint for the new official Helmholtz-wide Authentication and Authorization Infrastructure (Helmholtz AAI), 
derived from former [HDF AAI](http://cvs.data.kit.edu/hdf-aai/), 
is now online and available for services and identity providers via 
[login.helmholtz.de](https://login.helmholtz.de/) and the respective
[development instance](https://login-dev.helmholtz.de).
It was drafted by [FZJ](https://fz-juelich.de) and [KIT](https://www.kit.edu/), 
and is compatible to the [AARC blueprint](https://aarc-project.eu/architecture/).
<!--more-->

The establishment of such Helmholtz-wide AAI service is an 
organizational and technological key component to allow common access to IT services.
Based on this infrastructure, cloud services, e.g. for data management, collaboration and
scientific work are partially already in use as prototypes and 
[will go into production by end of 2020]({% link roadmap/index.md %}).
The full establishment of such federated IT infrastructure in Helmholtz will allow
closer 
[partnering to European IT and research communities]({% link partners/index.md %})
such as 
[EGI](https://www.egi.eu) and 
[EOSC](https://www.eosc-portal.eu/).


<div class="alert alert-success">
  <h2 id="contact-us"><i class="fas fa-info-circle"></i> Comments or Suggestions?</h2>
  <p>
    Feel free to <a href="{% link contact.md %}">contact us</a>.
  </p>
</div>
