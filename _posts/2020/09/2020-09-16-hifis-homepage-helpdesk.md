---
title: "HIFIS Pages and Helpdesk launched"
title_image: spacex-uj3hvdfQujI-unsplash.jpg
data: 2020-09-16
authors:
  - jandt
  - huste
layout: blogpost
categories:
  - news
excerpt_separator: <!--more-->
---

#### Homepage and Documentation
The [HIFIS homepage](https://www.hifis.net) 
as well as the [HIFIS Documentation](https://www.hifis.net/doc) 
have been restructured and relaunched. 

The new workflow uses best-practices continuous integration and continuous delivery (CI/CD) workflow to streamline collaborative contribution to the homepage and the documentation.
<!--more--> 
The workflows are based on [Jekyll](https://jekyllrb.com/) and [MkDocs](https://www.mkdocs.org/). 

The corresponding repositories are hosted in Gitlab at HZDR and are publicly accessible. You can find them
[here for the HIFIS homepage](https://gitlab.hzdr.de/hifis/hifis.net) and
[here for the HIFIS Documentation](https://gitlab.hzdr.de/hifis/hifis-technical-documentation).

#### HIFIS Helpdesk
Along with the relaunch of the homepage, the new HIFIS-wide helpdesk has started, accessible via 
[direct link]({% link contact.md %}) and/or directly via email at 
[support@hifis.net](mailto:support@hifis.net). 

With this, you can contact [the HIFIS team]({% link team/index.md %}) through a unique and reliable communication channel.


#### Announcement Letters and News Feed
Also, you are invited to subscribe for our Announcement letter `announce@hifis.net` to never miss any upcoming events:

You can do this either [**by clicking on this link** and sending a subscription mail](mailto:sympa@desy.de?subject=sub%20hifis-announce) or 
[**by subscribing here** - you can also unsubscribe here](https://lists.desy.de/sympa/subscribe/hifis-announce).

To stay updated on general HIFIS news, you are further invited to check out 
[our RSS news feed]({{ site.feed.collections.posts.path | relative_url }}) - 
besides, of course, regularly checking our website and **News Section!**



<div class="alert alert-success">
  <h2 id="contact-us"><i class="fas fa-info-circle"></i> Comments or Suggestions?</h2>
  <p>
    Feel free to <a href="{% link contact.md %}">contact us</a>.
  </p>
</div>
