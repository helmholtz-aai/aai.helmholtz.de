---
title: "Helmholtz AAI Policies finalised"
title_image: spacex-uj3hvdfQujI-unsplash.jpg
data: 2020-11-20
layout: blogpost
categories:
  - news
redirect_to:
  - https://hifis.net/news/2020/11/20/helmholtz-aai-policies
excerpt: >
    The Helmholtz AAI polcies have been finalised and approved by the HIFIS Coordination Board.

---
