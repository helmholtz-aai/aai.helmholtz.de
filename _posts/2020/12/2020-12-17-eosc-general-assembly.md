---
title: "Constituting General Assembly of EOSC Association"
title_image: spacex-uj3hvdfQujI-unsplash.jpg
data: 2020-12-17
authors:
  - jandt
layout: blogpost
categories:
  - news
excerpt: >
    In the constituting General Assembly of the EOSC association, the first President and Directors have been elected.

---

# EOSC Association General Assembly

In the [constituting General Assembly](https://www.eosc-portal.eu/news/eosc-association-general-assembly-take-place-tomorrow) of the EOSC association, the first President and Directors have been elected.

## President

**Karel Luyben** (TU Delft) has been elected as president with 119 out of 139 votes.


## Board of Directors

The Board of Directors consists of the following eight members:

#### For 3 year period

- Marialuisa Lavitrano (UNIMIB), Italy
- Suzanne Demouchel (CNRS), France
- Klaus Tochtermann (ZBW), Germany

#### For 2 year period

- Sarah Jones (GÉANT)
- Ignacio Blanquer (UPV), Spain

#### For 1 year period

- Wilhelm Widmark (SU), Sweden
- Bob Jones (CERN)
- Ronan Byrne (HEAnet), Ireland
