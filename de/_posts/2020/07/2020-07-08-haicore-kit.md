---
title: "Superschnelles AI-Rechensystem für HAICORE"
title_image: default
data: 2020-07-08
authors:
  - jandt
layout: blogpost
categories:
  - news
excerpt_separator: <!--more-->
lang_ref: 2020-07-08-haicore-kit
---

Am [KIT](https://www.kit.edu) wurde ein superschnelles Computersystem für
[Hemholtz Artificial Intelligence (AI)](https://www.helmholtz.ai/) Forschung installiert. 
Dies ist der erste Standort in Europa, an dem das mit InfiniBand verbundene NVIDIA DGX A100 AI-System in Betrieb genommen wurde, 
finanziert aus der Helmholtz AI Computing Resources Initiative (HAICORE). 
Der gemeinsame Zugang zu diesen Kapazitäten wird über den von HIFIS angebotenen AAI-Dienst hergestellt.
<!--more-->

<a type="button" class="btn btn-outline-primary btn-lg" href="https://www.kit.edu/kit/english/pi_2020_056_super-fast-ai-system-installed-at-kit.php">
  <i class="fas fa-external-link-alt"></i> Weitere Informationen
</a>
