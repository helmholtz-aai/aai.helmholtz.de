---
title: "HIFIS im EGI Newsletter"
title_image: default
data: 2020-06-08
authors:
  - jandt
layout: blogpost
categories:
  - news
excerpt_separator: <!--more-->
lang: de
lang_ref: 2020-06-19-hifis-in-the-egi-newsletter
---

_Helmholtz Federated IT Services - Promoting IT based science at all levels_,
so lautet der Titel unseres Artikels, der im Newsletter unserer Freunde von der
EGI-Foundation erschienen ist.
In dem EGI-Newsletter geben wir einen Überblick über die neue HIFIS-Plattform.
<!--more-->

<a type="button" class="btn btn-outline-primary btn-lg" href="https://www.egi.eu/about/newsletters/helmholtz-federated-it-services-promoting-it-based-science-at-all-levels/">
  <i class="fas fa-external-link-alt"></i> Weiterlesen
</a>
