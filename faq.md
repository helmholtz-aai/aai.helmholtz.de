---
title: <i class="fas fa-question-circle"></i> FAQ - Frequently Asked Questions
title_image: default
layout: default
redirect_to: https://hifis.net/faq#helmholtz-aai
excerpt:
    Collection of Frequently Asked Question (FAQ) about Helmholtz AAI.
---
