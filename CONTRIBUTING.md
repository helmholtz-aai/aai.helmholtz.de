---
layout: default
title: Contribution Guide
excerpt:
  Contribution guide for the HIFIS Software website.
---
# Contribution Guide

Hey, you are interested in contributing to the web-page of _HIFIS Software_?
That is great, your help is much appreciated!
Please take some time to read about the Coding Guidelines and
Best Practices that were agreed upon.
If you have questions do not hesitate to contact a group member
of the development team.
Also, we are happy to learn what you think about this web-page
and get feedback regarding how to improve its appearance and content.

Note:
In case you are just interested to know how to contribute with a
blog post to _HIFIS Software_ web-page you can find a 
[blog post](https://software.hifis.net/tutorials/2019/11/22/How-to-Create-a-new-Blog-Post "How to Create a New Blog Post?")
explaining the suggested workflow in the blog post section 
of the web-page.

## Contribution Workflow

### Workflow in a Nutshell

**1. Open an Issue, Share and Discuss the Topic**

Once you come up with an idea about how to contribute to _HIFIS Software_
web-page you can open a
[GitLab Issue](https://docs.gitlab.com/ce/user/project/issues/managing_issues.html#create-a-new-issue "Open a GitLab Issues")
and describe the bug you would like to fix, the feature you want to code
or the refactoring, testing or other quality assurance measures
you would like to implement.

Important:
Of course you need your own GitLab account of the
[HZDR GitLab](https://gitlab.hzdr.de/ "HZDR GitLab")
instance to do so as well as being added as a group member to the
development team.

Note:
At this point in time you would like to know more about the
branching model
[GitLab Flow](https://docs.gitlab.com/ce/topics/gitlab_flow.html "GitLab Flow")
that is supported by GitLab and the User-Interfaces of GitLab.

**2. Contribute with your Developments**

As soon as you would like to get to work you need to create your own
Git-branch.
Please give the branch a meaningful name according to the pattern
`<issue-number>-<branch-name>`.
It is good practice to check your code with static code checking tools
regularly as well as writing unit tests for your code.
Furthermore, when having unit tests in place as a safe guard you can
start refactoring your code to make it more readable, reusable,
testable, maintainable without changing the actual functionality
of your code.
Each Git-push to the remote Git repository will start a so called
_GitLab CI Pipeline_ to check that everything still works
as intended.

**3. The Review Process to Improve your Developments**

To enter the review process you need to open a
[GitLab Merge Request](https://docs.gitlab.com/ce/user/project/merge_requests/creating_merge_requests.html "GitLab Merge Request")
and assign it to your primary reviewer.
Give the merge request a meaningful title and description and
reference the original issue by stating `Closes #<issue-number>.`
in the description.
If applicable, give the title a name starting with `[WIP]`
to indicate that you are still working on it.
This allows previews and runs the GitLab CI/CD pipeline on your commits.
Also, set the `Progress::CanReview` label for the merge request,
so the others know you want to have it reviewed.

Please feel free to use GitLab's _Review Apps_ functionality to
view and test your branch.
It is an easy way to inspect your changes in a test / staging 
environment as well as involve external reviewers into the project.
When pushing your branch onto the remote repository your branch is
automatically deployed to _Review Apps_.
You can preview your latest deployments of your branch
by clicking the button _View app_ in the User-Interface of your 
Merge Request.
Alternatively, for branch _master_ you can open the _External URL_
in your browser by yourself:
`https://hifis-review-app.hzdr.de/review-apps/hifis-software-hifis-net/`.
(The automatically generated _External URLs_ for all other 
branches contain an additional sequence of numbers, lower case
letters and dashes as an URL-friendly replacement of the branch
name given.)

You need to authenticate to access these pages.
The credentials are as follows:
Username: `hifis`, Password: `HIFISReview!`.

Note:
Make sure that the corresponding _GitLab CI Pipeline_ finished
successfully after pushing your changes to the remote repository,
otherwise the changes to your branch will not yet be deployed to
_Review Apps_ as a new version.

After opening a _Merge Request_ discuss the changes suggested by your 
reviewers in dedicated discussion threads in GitLab and agree upon 
how to integrate these suggestions into your code to improve your
developments.

Important:
Please do not rely on contributions that are not reviewed or
that are only reviewed by yourself.
This is an important quality gate.

After integration has been done and _GitLab CI Pipeline_
finishes successfully you are nearly crossing the finishing line.

**4. Integrate your Developments**

Once all discussion threads are resolved and the _GitLab CI Pipeline_
finishes successfully the primary reviewer can merge your branch into
branch _master_.
Be sure, we appreciate your contribution very much and thank you for
the great collaboration.

## Code Guidelines and Best Practices

### Common Style Guides

If you are about to contribute to this web-site we recommend to adhere to
common style guides for each language used.
Since we are using Jekyll to generate static web sites we mostly stick
to Markdown / Kramdown, HTML, CSS / SCSS and JavaScript.
Jekyll itself is written in Ruby.

The following common style guides for each language used are just a
suggestion. We do not expect from you to rigorously comply to these
style guides. Nonetheless, they are a good starting point for
contributions according to Best Practices:

A first reference point is the
[Google Markdown Style Guide](https://github.com/google/styleguide/blob/gh-pages/docguide/style.md "Google Markdown Style Guide").
The
[Google HTML/CSS Style Guide](https://google.github.io/styleguide/htmlcssguide.html "Google HTML/CSS Style Guide").
is another good reference point if you are about to write HTML and CSS.
In order to generate CSS from SCSS there are the
[SASS Style Rules](https://sass-lang.com/documentation/style-rules "SASS Style Rules")
as well.
In terms of Javascript there is the
[Google JavaScript Style Guide](https://google.github.io/styleguide/jsguide.html "Google JavaScript Style Guide").
In the very unlikely case you are contributing to Jekyll or write your
own Jekyll plugins you should read also the
[Ruby Style Guide](https://rubystyle.guide/ "Ruby Style Guide").

This is a lot to read at the first glance, but it essentially saves you
and us time if you try to take these rules into account
right from the beginning, since these rule violations
might come up during the review process anyway.

### Style Checking Tools

Due to the fact that style guides are quite complex and to adhere to them
is a difficult task we recommend to consult dedicated tools to do the
checking for you.
Here comes the time saving into play.

| Static Code Analysis Tool | Language to Check |
|---|---|
| markdownlint | Markdown |
| HTML-Proofer | HTML|
| CSSLint | CSS |
| JSHint | JavaScript |
| Rubocop | Ruby |

#### markdownlint

[markdownlint](https://www.npmjs.com/package/markdownlint "markdownlint")
lets you check your Markdown. It is available as CLI-tool and
can be installed via npm and executed as follows:

```bash
$ npm install -g markdownlint
$ markdownlint path/to/markdown/folder/
```

#### HTML-Proofer

[HTML-Proofer](https://github.com/gjtorikian/html-proofer "HTML-Proofer")
lets you check your HTML. It is available as CLI-tool and
can be installed via gem and executed as follows:

```bash
$ gem install html-proofer
$ html-proofer path/to/html/folder/
```

#### CSSLint

[CSSLint](https://github.com/CSSLint/csslint/ "CSSLint")
lets you check your CSS. It is available as CLI-tool and
can be installed via npm and executed as follows:

```bash
$ npm install -g csslint
$ csslint path/to/css/folder/
```

#### JSHint

[JSHint](https://jshint.com/about/ "JSHint")
lets you check your JavaScript. It is available as CLI-tool and
can be installed via npm and executed as follows:

```bash
$ npm install -g jshint
$ jshint path/to/js/folder/
```

#### Rubocop

[Rubocop](https://github.com/rubocop-hq/rubocop "Rubocop")
lets you check your Ruby code. It is available as CLI-tool and
can be installed via gem and executed as follows:

```bash
$ gem install rubocop
$ rubocop path/to/ruby/code/folder/
```

### Specific Aspects in our Style Guide

#### User Interaction Considerations
##### Offer Alternatives to Hover Interaction
Keep in mind, that mobile devices like phones or tablets usually do not have the
ability to hover over an element on the website. Build alternatives for these
into your CSS and make sure they are used if the display is tablet-sized or
even smaller. Consult `_sass/_breakpoints.scss` for available breakpoints.

_More detailed discussion:_
Hovering is considered a good method to offer additional information without
forcing a context switch on the user (e.g. loading a new page or skipping to a
different part of the page).
Since for technical reasons this is not possible on mobile devices, an
alternative has to be considered for those. The usual approach is to entice the
user to click the element in question instead. To do so, it should be hinted to
the user that the element is clickable. If possible, use the websites default
link or button designs. In all other cases it can be achieved by either visual
styling (Since you can not use hover effects, consider subtle drop shadows) or
added badge icons
([Inspiration](https://www.flaticon.com/free-icons/hand-click)).

#### CSS
##### Avoid Magic Values
Values that show up in _CSS_ may carry a semantic meaning, e.g. colors of the
color palette or device sizes.
Use the provided _SASS_ variables or named CSS values instead of hard-coding
magic values. Expand the set of variables, if required.
Example:

```scss
@import "colors"
p {
    background-color: $color-helmholtz-blue;
    color: white;
}
```

instead of

```scss
p {
    background-color: #005AA0;
    color: #fff;
}
```

See the `_sass` folder for definitions, grouped by context.

#### HTML
##### Write Comments in _Liquid_ tags
Please use _Liquid_ comments in your HTML files to avoid them showing up in the
rendered version.
Example:

```liquid
<html>
    {% comment %}
        This text will not show up in the rendered HTML page.
        It serves a documentary purpose only.
    {% endcomment %}
</html>
```

instead of

```HTML
<html>
    <!-- This is an HTML comment. It will be preserved and show up in the 
    rendered page, cluttering and bloating the delivered code. -->
</html>
```
